# Mastodon Post Script

A tool to send messages to a Mastodon server

# SYNOPSIS

    usage: ( -r <recipiend account> ) ( -V <visibility> ) ( -s <secrets file> ) ( -m <media file> ) -- message-text

Arguments:

    message-text       The text of the posted message. Please note the `--`.

Options:

    --recipient|-r <account>
                       A recipient or mentioned Mastodon user (without @-sign). Can be
                       supplied multiple times.  
    --media|-m <file>  A media file to upload along with the message. Can be
                       supplied multiple times.  
    --visibility|-V <visibility>
                       public =   Visible to everyone, shown in public timelines.
                       unlisted = Visible to public, but not included in public timelines.
                       private =  Visible to followers only, and to any mentioned users.
                       direct =   Visible only to mentioned users.
    --secrets|-s <secrets file>
                       A file with secrets.                    
                       

# DESCRIPTION

**This program** uses `curl` to communicate with the Mastodon API.

Create a secrets file with these settings, and supply with the `-s` flag:

    # enter Mastodon server here, for example 'https://universeodon.com'
    export server=''
    
    # Get these details from your Mastodon account, on the server web GUI
    # (account -> develop -> new app)
    export client_key=''
    export client_secret=''
    export access_token=''
    

Please make sure `curl` and `jq` are installed and available in your `$PATH`.

# AUTHOR

Written by Per Weijnitz.

# REPORTING BUGS

[https://gitlab.com/perweij/mastodon-poster](https://gitlab.com/perweij/mastodon-poster)

# COPYRIGHT

This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.
